# GLTF Viewer

## État du projet

Le tp est terminé avec des modifications mineures pour simplifier la lecture. Le projet a été commencé avec une passe de post-process mais il n'est pas terminé en l'état.

### Modifications du TP

J'ai utilisé un struture avec `std::pair` pour récupérrer les attributs gltf :

```cpp
const std::vector<std::pair<std::string, GLuint>> attributesToGet = {
    {"POSITION", 0}, {"NORMAL", 1}, {"TEXCOORD_0", 2}};
```

Je passe par une classe `Pipeline` pour gerrer l'utilisation des shaders et des uniforms, cela me permet d'identifier facilement à quel shader appartiens quel uniform. Les IDs de ces derniers sont stockés à l'aide d'une hashmap.

### Les avancements du projet

J'ai réutilisé des éléments d'anciens projets OpenGL pour simplifier la création de framebuffer au travers de la classe `RenderTarget`. Cette dernière prend en entrée une description et crée les objets adéquats. Cette approche me permet simplement de gérrer du defered shading ou du post process. En l'état ce n'est cependant pas utilisé car je n'ai pas encore abstrait la boucle de rendu pour pouvoir switcher aisément de shaders.

```cpp
PipelineOutputDescription outputDescription = {
    { RenderTargetUsage::Color, RenderTargetType::Texture, RenderTargetDataType::FLOAT, RenderTargetChannels::RGBA, "Geometry_Albedo" },
    { RenderTargetUsage::Color, RenderTargetType::Texture, RenderTargetDataType::FLOAT, RenderTargetChannels::RGBA, "Geometry_Normal" },
    { RenderTargetUsage::Color, RenderTargetType::Texture, RenderTargetDataType::FLOAT, RenderTargetChannels::RGBA, "Geometry_LightSpacePosition" },
    { RenderTargetUsage::Color, RenderTargetType::RenderBuffer, RenderTargetDataType::UCHAR, RenderTargetChannels::RGBA, "EntityIdToColor", RenderTargetOperation::ReadPixel },
    { RenderTargetUsage::Depth, RenderTargetType::RenderBuffer, RenderTargetDataType::FLOAT, RenderTargetChannels::R, "Depth" }
};
```

Je souhaitais implémenter le Bloom ainsi que du deferred shading et des ombres, mais j'ai manqué d'organisation pour respecter la deadline. Ce sont des effets que j'ai déja implémenté par le passé mais que je souhaite réintégrer avec une plus belle architecture pour ce projet (mais passé la deadline).

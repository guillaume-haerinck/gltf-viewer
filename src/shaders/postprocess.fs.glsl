#version 330

in vec2 vTexCoords;

uniform sampler2D uBaseColorTexture;

out vec3 fColor;

void main()
{
    // TODO apply bloom etc
    fColor = texture(uBaseColorTexture, vTexCoords).rgb;
}

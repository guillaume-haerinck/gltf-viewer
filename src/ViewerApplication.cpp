#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>
#include <memory>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/images.hpp"
#include "debugbreak/debugbreak.h"
#include "utils/gltf.hpp"
#include "utils/pipeline.hpp"
#include "utils/render-target.hpp"

#include <stb_image_write.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
  {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;
  std::cout << "Loading GLTF model : " << m_gltfFilePath.string() << std::endl;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());
  if (!ret)
  {
    std::cerr << "[loadGltfFile] Error : " << err << "Warn : " << warn << std::endl;
    debug_break();
  }

  std::cout << "GLTF model loaded" << std::endl;
  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  glGenBuffers(model.buffers.size(), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i)
  {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
    glObjectLabel(GL_BUFFER, bufferObjects[i], model.buffers[i].name.size(), model.buffers[i].name.c_str());
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  std::cout << "Number of Buffers : " << bufferObjects.size() << std::endl;
  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> &meshIndexToVaoRange, const std::vector<std::pair<std::string, GLuint>> &attributesToGet)
{
  std::vector<GLuint> vertexArrayObjects;
  meshIndexToVaoRange.resize(model.meshes.size());

  for (size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx)
  {
    const tinygltf::Mesh &mesh = model.meshes[meshIdx];
    const unsigned int vaoOffset = vertexArrayObjects.size();
    vertexArrayObjects.resize(vaoOffset + mesh.primitives.size());
    meshIndexToVaoRange.push_back(VaoRange{vaoOffset, mesh.primitives.size()});

    glGenVertexArrays(mesh.primitives.size(), &vertexArrayObjects[vaoOffset]);
    for (size_t pIdx = 0; pIdx < mesh.primitives.size(); ++pIdx)
    {
      const auto &primitive = mesh.primitives[pIdx];
      const auto vao = vertexArrayObjects[vaoOffset + pIdx];
      glBindVertexArray(vao);
      glObjectLabel(GL_VERTEX_ARRAY, vao, mesh.name.size(), mesh.name.c_str());

      for (const auto &attributInfos : attributesToGet)
      {
        const auto iterator = primitive.attributes.find(attributInfos.first);
        if (iterator != end(primitive.attributes))
        {
          const int accessorIdx = (*iterator).second;
          const tinygltf::Accessor &accessor = model.accessors[accessorIdx];
          const tinygltf::BufferView &bufferView = model.bufferViews[accessor.bufferView];
          const int bufferIdx = bufferView.buffer;
          const tinygltf::Buffer bufferObject = model.buffers[bufferIdx];

          glEnableVertexAttribArray(attributInfos.second);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = bufferView.byteOffset + accessor.byteOffset;
          glVertexAttribPointer(attributInfos.second, accessor.type, accessor.componentType, GL_FALSE, bufferView.byteStride, (const void *)byteOffset);
        }
        else
        {
          std::cerr << "[createVertexArrayObjects] ERROR : Missing attribute from gltf file : " << attributInfos.first << std::endl;
          debug_break();
        }
      }

      // Index array if defined
      if (primitive.indices >= 0)
      {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        // Binding is enough for OpenGL to know that it needs this IBO for this VAO
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
    }
  }
  glBindVertexArray(0);
  std::cout << "Number of VAOs: " << vertexArrayObjects.size() << std::endl;
  return vertexArrayObjects;
}

int ViewerApplication::run()
{
  // Offscreen pass
  Pipeline offscreenPass(m_ShadersRootPath / "forward.vs.glsl", m_ShadersRootPath / "pbr_directional_light.fs.glsl");
  PipelineOutputDescription offScreenDescription = {
    { RenderTargetUsage::Color, RenderTargetType::Texture, RenderTargetDataType::FLOAT, RenderTargetChannels::RGBA, "Color" },
    { RenderTargetUsage::Depth, RenderTargetType::RenderBuffer, RenderTargetDataType::FLOAT, RenderTargetChannels::R, "Depth" }
  };
  RenderTarget offscreenTarget(offScreenDescription, glm::ivec2(m_nWindowWidth, m_nWindowHeight));

  // Postprocess pass, simply unbind the offscreen pass to render on screen
  // Pipeline postprocessPass(m_ShadersRootPath / "postprocess.vs.glsl", m_ShadersRootPath / "postprocess.fs.glsl");

  // Loading the glTF file
  tinygltf::Model model;
  loadGltfFile(model);

  // Compute bounds
  glm::vec3 bboxMin;
  glm::vec3 bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  const glm::vec3 center = (bboxMin + bboxMax) / 2.0f;
  const glm::vec3 diagonal = bboxMax - bboxMin;

  // Build projection matrix
  auto maxDistance = glm::length(diagonal);
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix = glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight, 0.001f * maxDistance, 1.5f * maxDistance);

  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
  if (m_hasUserCamera)
    cameraController->setCamera(m_userCamera);
  else
  {
    const auto up = glm::vec3(0, 1, 0);
    const auto eye = diagonal.z > 0 ? center + diagonal : center + 2.f * glm::cross(diagonal, up);
    cameraController->setCamera(Camera{eye, center, up});
  }

  // Init light parameters
  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);
  bool lightFromCamera = false;
  bool applyOcclusion = true;

  // Load textures
  const auto textureObjects = createTextureObjects(model);
  const auto whiteTexture = createWhiteTexture();

  // Creation of Vertex Buffer Objects
  const auto bufferObjects = createBufferObjects(model);

  // Creation of Vertex Array Objects and Index buffer if exists
  std::vector<VaoRange> meshIndexToVaoRange;
  const std::vector<std::pair<std::string, GLuint>> attributesToGet = {
      {"POSITION", 0}, {"NORMAL", 1}, {"TEXCOORD_0", 2}};
  const auto vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange, attributesToGet);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  offscreenPass.bind();
  //offscreenTarget.bind();

  const auto bindOffscreenPassMaterial = [&](const auto materialIndex) {
    if (materialIndex >= 0)
    {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      auto textureObject = whiteTexture;
      
      const auto color = glm::make_vec4(&pbrMetallicRoughness.baseColorFactor[0]);
      const auto emissive = glm::make_vec3(&material.emissiveFactor[0]);

      offscreenPass.setUniformVec4f("uBaseColorFactor", color);
      offscreenPass.setUniform1f("uMetallicFactor", static_cast<float>(pbrMetallicRoughness.metallicFactor));
      offscreenPass.setUniform1f("uRoughnessFactor", static_cast<float>(pbrMetallicRoughness.roughnessFactor));
      offscreenPass.setUniformVec3f("uEmissiveFactor", emissive);
      offscreenPass.setUniform1f("uOcclusionStrength", static_cast<float>(material.occlusionTexture.strength));

      if (pbrMetallicRoughness.baseColorTexture.index >= 0)
      {
        textureObject = whiteTexture;
        const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
        if (texture.source >= 0)
          textureObject = textureObjects[texture.source];
      }
      offscreenPass.setTexture("uBaseColorTexture", textureObject, 0);

      if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0)
      {
        textureObject = 0;
        const auto &texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
        if (texture.source >= 0)
          textureObject = textureObjects[texture.source];
      }
      offscreenPass.setTexture("uMetallicRoughnessTexture", textureObject, 1);

      if (material.emissiveTexture.index >= 0)
      {
        textureObject = 0;
        const auto &texture = model.textures[material.emissiveTexture.index];
        if (texture.source >= 0)
          textureObject = textureObjects[texture.source];
      }
      offscreenPass.setTexture("uEmissiveTexture", textureObject, 2);

      if (material.occlusionTexture.index >= 0)
      {
        textureObject = whiteTexture;
        const auto &texture = model.textures[material.occlusionTexture.index];
        if (texture.source >= 0)
          textureObject = textureObjects[texture.source];
      }
      offscreenPass.setTexture("uOcclusionTexture", textureObject, 3);
    }
    else
    {
      // Apply default material
      // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-material
      // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-pbrmetallicroughness3
      offscreenPass.setTexture("uBaseColorTexture", whiteTexture, 0);
      offscreenPass.setTexture("uMetallicRoughnessTexture", 0, 1);
      offscreenPass.setTexture("uEmissiveTexture", 0, 2);
      offscreenPass.setTexture("uOcclusionTexture", 0, 3);

      offscreenPass.setUniformVec4f("uBaseColorFactor", glm::vec4(1));
      offscreenPass.setUniform1f("uMetallicFactor", 1.f);
      offscreenPass.setUniform1f("uRoughnessFactor", 1.f);
      offscreenPass.setUniformVec3f("uEmissiveFactor", glm::vec3(0.f));
      offscreenPass.setUniform1f("uOcclusionStrength", 0.f);
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();


    offscreenPass.setUniformVec3f("uLightIntensity", lightIntensity);
    offscreenPass.setUniform1i("uApplyOcclusion", applyOcclusion);
    if (lightFromCamera)
    {
      offscreenPass.setUniformVec3f("uLightDirection", glm::vec3(0, 0, 1));
    }
    else
    {
      const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
      offscreenPass.setUniformVec3f("uLightDirection", lightDirection);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const auto &node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
          if (node.mesh >= 0)
          {
            const auto modelViewMatrix = viewMatrix * modelMatrix;
            const auto modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            const auto normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

            offscreenPass.setUniformMat4f("uModelViewProjMatrix", modelViewProjectionMatrix);
            offscreenPass.setUniformMat4f("uModelViewMatrix", modelViewMatrix);
            offscreenPass.setUniformMat4f("uNormalMatrix", normalMatrix);

            const auto &mesh = model.meshes[node.mesh];
            const VaoRange &vaoRange = meshIndexToVaoRange[node.mesh];
            for (size_t primIdx = 0; primIdx < mesh.primitives.size(); ++primIdx)
            {
              const auto &primitive = mesh.primitives[primIdx];
              bindOffscreenPassMaterial(primitive.material);
              const auto vao = vertexArrayObjects[vaoRange.begin + primIdx];
              glBindVertexArray(vao);

              if (primitive.indices >= 0)
              {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, accessor.count, accessor.componentType, (const void *)byteOffset);
              }
              else
              {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, accessor.count);
              }
            }
          }
          // Draw recursivly
          for (const auto &children : node.children)
            drawNode(children, modelMatrix);
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0)
    {
      for (const auto &node : model.scenes[model.defaultScene].nodes)
        drawNode(node, glm::mat4(1));
    }
  };

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose(); ++iterationCount)
  {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    const bool bRenderToFrame = !m_OutputPath.empty();
    if (bRenderToFrame)
    {
      std::vector<unsigned char> pixels(m_nWindowHeight * m_nWindowWidth * 3);
      renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {
        drawScene(cameraController->getCamera());
      });
      const auto strPath = m_OutputPath.string();
      flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
      stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
      break; // Exit the application
    }

    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen))
      {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
                    camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
                    camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
                    camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
                    camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard"))
        {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
      }

      static int cameraControllerType = 0;
      const auto cameraControllerTypeChanged =
          ImGui::RadioButton("Trackball", &cameraControllerType, 0) ||
          ImGui::RadioButton("First Person", &cameraControllerType, 1);

      if (cameraControllerTypeChanged)
      {
        const auto currentCamera = cameraController->getCamera();
        if (cameraControllerType == 0)
        {
          cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
        }
        else
        {
          cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), maxDistance);
        }
        cameraController->setCamera(currentCamera);
      }

      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen))
      {
        static float lightTheta = 0.f;
        static float lightPhi = 0.f;

        if (ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) ||
            ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>()))
        {
          const auto sinPhi = glm::sin(lightPhi);
          const auto cosPhi = glm::cos(lightPhi);
          const auto sinTheta = glm::sin(lightTheta);
          const auto cosTheta = glm::cos(lightTheta);
          lightDirection = glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }

        static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
            ImGui::InputFloat("intensity", &lightIntensityFactor))
        {
          lightIntensity = lightColor * lightIntensityFactor;
        }

        ImGui::Checkbox("light from camera", &lightFromCamera);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
      }

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus)
    {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // Clean up allocated GL data
  glDeleteBuffers(bufferObjects.size(), bufferObjects.data());
  glDeleteVertexArrays(vertexArrayObjects.size(), vertexArrayObjects.data());
  glDeleteTextures(1, &whiteTexture);
  glDeleteTextures(textureObjects.size(), textureObjects.data());

  return 0;
}

GLuint ViewerApplication::createWhiteTexture() const
{
  GLuint whiteTexture = 0;

  // Create white texture for object with no base color texture
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[] = {1, 1, 1, 1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  return whiteTexture;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const
{
  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  // default sampler:
  // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#texturesampler
  // "When undefined, a sampler with repeat wrapping and auto filtering should
  // be used."
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  for (size_t i = 0; i < model.textures.size(); ++i)
  {
    const auto &texture = model.textures[i];
    assert(texture.source >= 0);
    const auto &image = model.images[texture.source];

    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR)
    {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }
  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
                                     uint32_t height, const fs::path &gltfFile,
                                     const std::vector<float> &lookatArgs, const std::string &vertexShader,
                                     const std::string &fragmentShader, const fs::path &output) : m_nWindowWidth(width),
                                                                                                  m_nWindowHeight(height),
                                                                                                  m_AppPath{appPath},
                                                                                                  m_AppName{m_AppPath.stem().string()},
                                                                                                  m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
                                                                                                  m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
                                                                                                  m_gltfFilePath{gltfFile},
                                                                                                  m_OutputPath{output}
{
  if (!lookatArgs.empty())
  {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
               glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
               glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}

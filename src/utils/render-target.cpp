#include "render-target.hpp"

#include <glad/glad.h>
#include <iostream>

#include "gldebug/gl-debug.h"

GLenum renderTargetChannelsToOpenGLInternalFormat(RenderTargetChannels channels, RenderTargetDataType dataType)
{
    if (dataType == RenderTargetDataType::UCHAR)
    {
        switch (channels)
        {
        case RenderTargetChannels::R:
            return GL_R8;
        case RenderTargetChannels::RG:
            return GL_RG8;
        case RenderTargetChannels::RGB:
            return GL_RGB8;
        case RenderTargetChannels::RGBA:
            return GL_RGBA8;
        default:
            break;
        }
    }
    else if (dataType == RenderTargetDataType::FLOAT)
    {
        switch (channels)
        {
        case RenderTargetChannels::R:
            return GL_R16F;
        case RenderTargetChannels::RG:
            return GL_RG16F;
        case RenderTargetChannels::RGB:
            return GL_RGB16F;
        case RenderTargetChannels::RGBA:
            return GL_RGBA16F;
        default:
            break;
        }
    }

    assert(false && "Unknown RenderTargetChannels type !");
    return 0;
}

GLenum renderTargetChannelsToOpenGLBaseFormat(RenderTargetChannels channels)
{
    switch (channels)
    {
    case RenderTargetChannels::R:
        return GL_RED;
    case RenderTargetChannels::RG:
        return GL_RG;
    case RenderTargetChannels::RGB:
        return GL_RGB;
    case RenderTargetChannels::RGBA:
        return GL_RGBA;
    default:
        break;
    }

    assert(false && "Unknown RenderTargetChannels type !");
    return 0;
}

GLenum renderTargetDataTypeToOpenGLBaseType(RenderTargetDataType dataType)
{
    switch (dataType)
    {
    case RenderTargetDataType::UCHAR:
        return GL_UNSIGNED_BYTE;
    case RenderTargetDataType::USHORT:
        return GL_UNSIGNED_SHORT;
    case RenderTargetDataType::UINT:
        return GL_UNSIGNED_INT;
    case RenderTargetDataType::FLOAT:
        return GL_FLOAT;
    default:
        break;
    }

    assert(false && "Unknown RenderTargetDataType !");
    return 0;
}

RenderTarget::RenderTarget(const PipelineOutputDescription &description, const glm::ivec2 &size)
{
    // Create new framebuffer
    unsigned int fb;
    GLCall(glGenFramebuffers(1, &fb));
    GLCall(glBindFramebuffer(GL_FRAMEBUFFER, fb));

    unsigned int slot = 0;
    for (const auto &target : description)
    {
        unsigned int rbo;
        unsigned int textureId;

        switch (target.type)
        {
        case RenderTargetType::Texture:
            GLCall(glGenTextures(1, &textureId));
            GLCall(glBindTexture(GL_TEXTURE_2D, textureId));
            GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
            GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
            GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
            GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
            textureIds.push_back(textureId);
            break;

        case RenderTargetType::RenderBuffer:
            GLCall(glGenRenderbuffers(1, &rbo));
            GLCall(glBindRenderbuffer(GL_RENDERBUFFER, rbo));
            renderBufferIds.push_back(rbo);
            break;

        default:
            std::cerr << "[createRenderTarget] Unknown render target type" << std::endl;
            debug_break();
            assert(false);
            break;
        }

        switch (target.usage)
        {
        case RenderTargetUsage::Color:
            if (target.type == RenderTargetType::Texture)
            {
                GLCall(glTexImage2D(GL_TEXTURE_2D, 0, renderTargetChannelsToOpenGLInternalFormat(target.channels, target.dataType), size.x, size.y, 0, renderTargetChannelsToOpenGLBaseFormat(target.channels), renderTargetDataTypeToOpenGLBaseType(target.dataType), 0));
                GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + slot, GL_TEXTURE_2D, textureId, 0));
            }
            else if (target.type == RenderTargetType::RenderBuffer)
            {
                GLCall(glRenderbufferStorage(GL_RENDERBUFFER, renderTargetChannelsToOpenGLInternalFormat(target.channels, target.dataType), size.x, size.y));
                GLCall(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + slot, GL_RENDERBUFFER, rbo));
            }
            slot++;
            break;

        case RenderTargetUsage::Depth:
            if (target.type == RenderTargetType::Texture)
            {
                GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, size.x, size.y, 0, GL_DEPTH_COMPONENT, renderTargetDataTypeToOpenGLBaseType(target.dataType), 0));
                GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureId, 0));
            }
            else if (target.type == RenderTargetType::RenderBuffer)
            {
                GLCall(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, size.x, size.y));
                GLCall(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo));
            }
            break;

        case RenderTargetUsage::DepthStencil:
            if (target.type == RenderTargetType::Texture)
            {
                GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, size.x, size.y, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0));
                GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, textureId, 0));
            }
            else if (target.type == RenderTargetType::RenderBuffer)
            {
                GLCall(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, size.x, size.y));
                GLCall(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo));
            }
            break;

        default:
            std::cerr << "[createRenderTarget] Unknown render target usage" << std::endl;
            debug_break();
            assert(false);
            break;
        }
    }

    // Attach color targets to framebuffer
    if (slot == 0)
    {
        GLCall(glDrawBuffers(0, GL_NONE));
    }
    else
    {
        std::vector<unsigned int> attachments(slot);
        for (unsigned int i = 0; i < slot; i++)
        {
            attachments.at(i) = GL_COLOR_ATTACHMENT0 + i;
        }
        GLCall(glDrawBuffers(slot, attachments.data()));
    }

    // Check for errors
    auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        std::string detail = "";
        switch (status)
        {
        case GL_FRAMEBUFFER_UNDEFINED:
            detail = "undefined";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            detail = "Incomplete attachment";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            detail = "Incomplete missing attachment";
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED:
            detail = "unsupported";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            detail = "Incomplete multisample";
            break;
        default:
            break;
        }

        std::cerr << "[createRenderTarget] FrameBuffer is not complete : " << detail << std::endl;
        debug_break();
        assert(false);
    }

    frameBufferId = fb;

    // Unbind
    GLCall(glBindRenderbuffer(GL_RENDERBUFFER, 0));
    GLCall(glBindTexture(GL_TEXTURE_2D, 0));
    GLCall(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

RenderTarget::~RenderTarget()
{
    GLCall(glDeleteTextures(textureIds.size(), textureIds.data()));
    GLCall(glDeleteBuffers(renderBufferIds.size(), renderBufferIds.data()));
    GLCall(glDeleteFramebuffers(1, &frameBufferId));
}

void RenderTarget::bind()
{
    GLCall(glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId));
}

void RenderTarget::unbind()
{
    GLCall(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

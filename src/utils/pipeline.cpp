#include "pipeline.hpp"

#include "gldebug/gl-debug.h"
#include <iostream>
#include <fstream>

Pipeline::Pipeline(const fs::path &vertexshaderPath, const fs::path &fragmentshaderPath)
{
	/*
        static auto extToShaderType =
        std::unordered_map<std::string, std::pair<GLenum, std::string>>(
            {{".vs", {GL_VERTEX_SHADER, "vertex"}},
            {".fs", {GL_FRAGMENT_SHADER, "fragment"}},
            {".gs", {GL_GEOMETRY_SHADER, "geometry"}},
            {".cs", {GL_COMPUTE_SHADER, "compute"}}});

    const auto ext = shaderPath.stem().extension();
    const auto it = extToShaderType.find(ext.string());
    if (it == end(extToShaderType))
    {
        std::cerr << "Unrecognized shader extension " << ext << std::endl;
        throw std::runtime_error("Unrecognized shader extension " + ext.string());
    }
    */

	// ------------------ Vertex shader
	unsigned int vs;
	int success;
	char infoLog[512];
	{
		std::string vsSource = readFile(vertexshaderPath.string());
		const char *vsSourceCstr = vsSource.c_str();
		vs = glCreateShader(GL_VERTEX_SHADER);
		GLCall(glShaderSource(vs, 1, &vsSourceCstr, NULL));
		GLCall(glCompileShader(vs));

		// Check compilation
		GLCall(glGetShaderiv(vs, GL_COMPILE_STATUS, &success));
		glObjectLabel(GL_SHADER, vs, vertexshaderPath.filename().string().size(), vertexshaderPath.filename().string().c_str());
		if (!success)
		{
			GLCall(glGetShaderInfoLog(vs, 512, NULL, infoLog));
			std::cerr << "[VertexShader] Compilation failed : " << infoLog << std::endl;
			debug_break();
		}
	}

	// ------------------ Fragment shader
	unsigned int fs;
	{
		std::string fsSource = readFile(fragmentshaderPath.string());
		const char *fsSourceCstr = fsSource.c_str();
		fs = glCreateShader(GL_FRAGMENT_SHADER);
		GLCall(glShaderSource(fs, 1, &fsSourceCstr, NULL));
		GLCall(glCompileShader(fs));

		// Check compilation
		int success;
		GLCall(glGetShaderiv(fs, GL_COMPILE_STATUS, &success));
		glObjectLabel(GL_SHADER, fs, fragmentshaderPath.filename().string().size(), fragmentshaderPath.filename().string().c_str());
		if (!success)
		{
			GLCall(glGetShaderInfoLog(fs, 512, NULL, infoLog));
			std::cerr << "[FragmentShader] Compilation failed : " << infoLog << std::endl;
			debug_break();
		}
	}

	// ------------------ Pipeline
	{
		m_pipelineID = glCreateProgram();
		GLCall(glAttachShader(m_pipelineID, vs));
		GLCall(glAttachShader(m_pipelineID, fs));
		GLCall(glLinkProgram(m_pipelineID));

		// Check compilation
		GLCall(glGetProgramiv(m_pipelineID, GL_LINK_STATUS, &success));
		std::string name = "[Pipeline] " + vertexshaderPath.filename().string() + " " + fragmentshaderPath.filename().string();
		glObjectLabel(GL_PROGRAM, m_pipelineID, name.size(), name.c_str());
		if (!success)
		{
			GLCall(glGetProgramInfoLog(m_pipelineID, 512, NULL, infoLog));
			std::cout << "[Pipeline] Link failed : " << infoLog << std::endl;
			debug_break();
		}

		// Delete useless data
		GLCall(glDeleteShader(vs));
		GLCall(glDeleteShader(fs));
	}
}

Pipeline::~Pipeline()
{
	GLCall(glDeleteProgram(m_pipelineID));
}

void Pipeline::bind()
{
	GLCall(glUseProgram(m_pipelineID));
}

void Pipeline::unbind()
{
	GLCall(glUseProgram(0));
}

void Pipeline::setTexture(const std::string uniformName, uint8_t textureId, uint8_t textureSlot)
{
	GLCall(glActiveTexture(GL_TEXTURE0 + textureSlot));
	GLCall(glBindTexture(GL_TEXTURE_2D, textureId));
	GLCall(glUniform1i(getUniformLocation(uniformName), textureSlot));
}

void Pipeline::setUniformMat4f(const std::string &uniformName, const glm::mat4x4 &mat)
{
	GLCall(glUniformMatrix4fv(getUniformLocation(uniformName), 1, GL_FALSE, &mat[0][0]));
}

void Pipeline::setUniformVec3f(const std::string &uniformName, const glm::vec3 &vector)
{
	GLCall(glUniform3f(getUniformLocation(uniformName), vector.x, vector.y, vector.z));
}

void Pipeline::setUniformVec4f(const std::string& uniformName, const glm::vec4& vector)
{
	GLCall(glUniform4f(getUniformLocation(uniformName), vector.x, vector.y, vector.z, vector.a));
}

void Pipeline::setUniform1f(const std::string &uniformName, float value)
{
	GLCall(glUniform1f(getUniformLocation(uniformName), value));
}

void Pipeline::setUniform1i(const std::string& uniformName, int value)
{
	GLCall(glUniform1i(getUniformLocation(uniformName), value));
}

int Pipeline::getUniformLocation(const std::string &name)
{
	if (m_uniformLocationCache.find(name) != m_uniformLocationCache.end())
	{
		return m_uniformLocationCache[name];
	}

	GLCall(int location = glGetUniformLocation(m_pipelineID, name.c_str()));
	if (location == -1)
	{
		std::cerr << "[Shader] uniform " << name << " doesn't exist or is not used (compiled out) !" << std::endl;
		// debug_break();
	}
	m_uniformLocationCache[name] = location;
	return location;
}

std::string Pipeline::readFile(const std::string &filepath)
{
	// Open file
	std::ifstream stream(filepath);
	if (!stream.is_open())
	{
		std::cerr << "Failed to open file : " << filepath << std::endl;
		return "";
	}

	// Read line by line and put it in a string
	std::string str = "";
	std::string tempLine = "";
	while (getline(stream, tempLine))
	{
		str += tempLine + '\n';
	}
	stream.close();
	return str;
}

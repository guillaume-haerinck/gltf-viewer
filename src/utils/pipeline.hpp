#pragma once

#include <glad/glad.h>
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>
#include "filesystem.hpp"

class Pipeline
{
public:
	Pipeline(const fs::path &vertexshaderPath, const fs::path &fragmentshaderPath);
	~Pipeline();

	void bind();
	void unbind();

	void setTexture(const std::string uniformName, uint8_t textureId, uint8_t textureSlot);
	void setUniformMat4f(const std::string& uniformName, const glm::mat4x4& mat);
	void setUniformVec3f(const std::string& uniformName, const glm::vec3& vector);
	void setUniformVec4f(const std::string& uniformName, const glm::vec4& vector);
	void setUniform1f(const std::string& uniformName, float value);
	void setUniform1i(const std::string& uniformName, int value);

private:
	int getUniformLocation(const std::string& name);
	std::string readFile(const std::string& filepath);

private:
	GLuint m_pipelineID;
	std::unordered_map<std::string, int> m_uniformLocationCache;
};



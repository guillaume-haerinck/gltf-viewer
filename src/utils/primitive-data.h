#pragma once

#include <glm/glm.hpp>

/**
 * @brief Geometry data for a square. Center of geometry is at v2, at bottom left.
 */
namespace squareData {
    //  v1-------v0
    //  |         |
    //  |         |
    //  |         |
    //  v2-------v3
    const glm::vec3 positions[] = {
        // v0,v1,v2,v3
        glm::vec3(1, 1, 0), glm::vec3(-1, 1, 0), glm::vec3(-1, -1, 0), glm::vec3(1, -1, 0)
    };

    //   X---------X
    //  /|        /|
    //   |         |
    //   |         |
    //   X---------X
    //  /         /
    const glm::vec3 normals[] = {
        glm::vec3(0, 0, 1), glm::vec3(0, 0, 1), glm::vec3(0, 0, 1), glm::vec3(0, 0, 1)
    };

    // 0;1 ----- 1;1
    //  |         |
    //  |         |
    //  |         |
    // 0;0 ----- 1;0
    const glm::vec2 texCoords[] = {
        glm::vec2(1, 1), glm::vec2(0, 1), glm::vec2(0, 0), glm::vec2(1, 0)
    };

    const unsigned char indices[] = {
        0, 1, 2,   2, 3, 0
    };
}
